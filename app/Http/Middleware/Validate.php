<?php

namespace App\Http\Middleware;
use Firebase\JWT\JWT;

use Closure;

class Validate {
    public function handle($request, Closure $next) {
        if ($request->header('Authorization')) {
            $jwt = $request->header('Authorization');
            $secretkey = "totitoldi";
            try {
                $decoded = JWT::decode($jwt, $secretkey, array('HS256'));
                $exp = $decoded->exp;
                if (time() < $exp) {
                    return $next($request);
                } else
                    return response()->json(['error' => 'The expired token'], 403);   
            } catch (\Exception $e) {
                return response()->json(['error' => 'The expired token'], 403);   
            }
        } else
            return response()->json(['error' => 'The Authorization header is needed to access this route'], 403);      
    }
}

